#!/bin/bash

# Validate input data
if [ -z "${ENVIRONMENT}" ]; then
    printf "ERROR: ENVIRONMENT is not defined, Please set environment variable ENVIRONMENT with appropriate value (possible values are dev, qa, prod).";
    exit 1;
fi

if [ -z "${SERVICE_PRINCIPAL_APP_ID}" ]; then
    printf "ERROR: SERVICE PRINCIPAL APP ID is not defined, Please set environment variable SERVICE_PRINCIPAL_APP_ID";
    exit 1;
fi

if [ -z "${SERVICE_PRINCIPAL_SECRET}" ]; then
    printf "ERROR: SERVICE PRINCIPAL SECRET is not defined, Please set environment variable SERVICE_PRINCIPAL_SECRET";
    exit 1;
fi

if [ -z "${AZURE_TENANT_ID}" ]; then
    printf "ERROR: AZURE TENANT ID is not defined, Please set environment variable AZURE_TENANT_ID";
    exit 1;
fi

if [ -z "${SUBSCRIPTION_ID}" ]; then
    printf "ERROR: SUBSCRIPTION ID is not defined, Please set environment variable SUBSCRIPTION_ID";
    exit 1;
fi

if [ -z "${LOCATION}" ]; then
    printf "ERROR: LOCATION is not defined, Please set environment variable LOCATION";
    exit 1;
fi

echo "> Logging into AZ CLI"
az login --service-principal -u $SERVICE_PRINCIPAL_APP_ID -p $SERVICE_PRINCIPAL_SECRET --tenant $AZURE_TENANT_ID

echo "> Logging into PULUMI CLI"
pulumi login  --non-interactive

if pulumi stack select $PULUMI_ORGANISATION_NAME/$ENVIRONMENT ; then
    echo "Pulumi stack $ENVIRONMENT already exists";

    # pulumi stack change-secrets-provider passphrase
    pulumi config set azure:clientId $SERVICE_PRINCIPAL_APP_ID
    pulumi config set azure:clientSecret $SERVICE_PRINCIPAL_SECRET --secret
    pulumi config set azure:tenantId $AZURE_TENANT_ID
    pulumi config set azure:subscriptionId $SUBSCRIPTION_ID
    pulumi config set azure:location $LOCATION
    pulumi config set azure-native:clientId $SERVICE_PRINCIPAL_APP_ID
    pulumi config set azure-native:clientSecret $SERVICE_PRINCIPAL_SECRET --secret
    pulumi config set azure-native:tenantId $AZURE_TENANT_ID
    pulumi config set azure-native:subscriptionId $SUBSCRIPTION_ID
    pulumi config set azure-native:location $LOCATION
    
    # Terraform provider needs the ARM template variable set. https://github.com/pulumi/pulumi/issues/3931
    export ARM_CLIENT_ID=$SERVICE_PRINCIPAL_APP_ID
    export ARM_CLIENT_SECRET=$SERVICE_PRINCIPAL_SECRET
    export ARM_TENANT_ID=$AZURE_TENANT_ID
    export ARM_SUBSCRIPTION_ID=$SUBSCRIPTION_ID
    
    echo "> Decommission $ENVIRONMENT stack"
    pulumi destroy -y
    
    if [ ! $? -eq 0 ]; then
        exit 1;
    fi

    echo "> Delete pulumi reference to $ENVIRONMENT stack"
    pulumi stack rm $PULUMI_ORGANISATION_NAME/$ENVIRONMENT -y
else 
    echo "Pulumi stack $ENVIRONMENT does not exist";
fi