function connect_to_kubernetes_cluster {

  echo "Connecting to Kubernetes cluster";

  if [ -z "${SERVICE_PRINCIPAL_APP_ID}" ]; then
    echo "ERROR: SERVICE PRINCIPAL APP ID is not defined, Please set environment variable SERVICE_PRINCIPAL_APP_ID";
    exit 1;
  fi

  if [ -z "${SERVICE_PRINCIPAL_SECRET}" ]; then
    echo "ERROR: SERVICE PRINCIPAL SECRET is not defined, Please set environment variable SERVICE_PRINCIPAL_SECRET";
    exit 1;
  fi

  if [ -z "${AZURE_TENANT_ID}" ]; then
    echo "ERROR: AZURE TENANT ID is not defined, Please set environment variable AZURE_TENANT_ID";
    exit 1;
  fi

  echo "> Logging into AZ CLI"
  az login --service-principal -u $SERVICE_PRINCIPAL_APP_ID -p $SERVICE_PRINCIPAL_SECRET --tenant $AZURE_TENANT_ID

  echo "> Authenticate to AKS cluster $clusterName in resource group $resourceGroupName"
  local resourceGroupName=$(pulumi stack output -s $PULUMI_ORGANISATION_NAME/platform-kubernetes-cluster/$ENVIRONMENT ResourceGroupName)
  local clusterName=$(pulumi stack output -s $PULUMI_ORGANISATION_NAME/platform-kubernetes-cluster/$ENVIRONMENT ClusterName)
  az aks get-credentials --resource-group $resourceGroupName --name $clusterName
  if [ ! $? -eq 0 ]; then
    echo "ERROR! Unable to authenticate to Azure Kubernest cluster";
    exit 1;
  fi

  echo "> Kubectl auth"
  kubelogin convert-kubeconfig -l azurecli
  if [ ! $? -eq 0 ]; then
    echo "ERROR! Unable to verify kube creds via Azure CLI";
    exit 1;
  fi

}

function validate_kubernetes_namespace {

  local namespace=$1  

  connect_to_kubernetes_cluster

  echo "> Ensure $namespace kubernetes namespace exists"
  kubectl get ns $namespace
  if [ ! $? -eq 0 ]; then
    kubectl create ns $namespace

    if [ ! $? -eq 0 ]; then
      echo "Unable to create namespace";
      exit 1;
    fi
  fi
   
}

function get_version_from_manifest {  
  local packageVersion=$(head -1 version.txt)
  echo $packageVersion
}

function increment_version_manifest {
  local currentVersion=$1

  local majorVersion=$(echo " $currentVersion" | cut -d "." -f 1)
  local minorVersion=$(echo " $currentVersion" | cut -d "." -f 2)
  local buildNumber=$(echo " $currentVersion" | cut -d "." -f 3)
  buildNumber=$(echo "$(($buildNumber + 1))")

  git pull --no-rebase

  local newPackageVersion=$majorVersion.$minorVersion.$buildNumber
  echo "${newPackageVersion}" > version.txt

  git add version.txt
  git commit -m "[skip ci] Updating version.txt with latest build number."
  git push
}
